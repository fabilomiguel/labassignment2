//Fabilo Miguel de Lima Silva 2235291
package application;

import vehicles.Bicycle;

public class Bikestore {

    public static void main(String[] args)
    {
        Bicycle[] vitrine = new Bicycle[4];  
         
        vitrine[0] = new Bicycle("Caloi", 7, 21.5);
        vitrine[1] = new Bicycle("Trek", 15, 36.5);
        vitrine[2] = new Bicycle("Specialized", 21, 40.0);
        vitrine[3] = new Bicycle("Giant", 5, 35.5);

        for(int i=0; i<vitrine.length; i++)
        {
            System.out.print(vitrine[i]);
            System.out.println("");
        }
    }
}
