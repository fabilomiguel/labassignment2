//Fabilo Miguel de Lima Silva 2235291
package vehicles;

public class Bicycle
{
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    //getters
    public String getManufacturer()
    {
        return this.manufacturer;
    }

    public int getnumberGears()
    {
        return this.numberGears;
    }
    public double getmaxSpeed()
    {
        return this.maxSpeed;
    }

    //constructor
    public Bicycle(String manufacturer, int numberGears, double maxSpeed)
    {
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    //toString override
    public String toString()
    {
        return  "Manufacturer: " +manufacturer+", Number of Gears: "+numberGears+", MaxSpeed: "+maxSpeed;
    }
}